# Assignment 9: SOLID Principles #

### Breaking Code Down Using Open-Close, Single Responsibility, and Interface Segragation  Principles ###



#### Tyler Gates - Created the worker and superworker class .java files

* Both files, worker and superworker, utilize the principle of single responsibility. (Even though they two responsibilities)

* The other two principles do not apply to my portion of the work.
 
#### Kaitlyn West - Created Robot.java file

* Open Close Principle - The Robot class is seperated from the main code (ThreeSolidMain.java) and is opened to extension but closed to modification. The Robot class only implements the iWorker class and that is all the robot is set to do (robots do not eat). 

* Single Responsibility Principle - The robot in the Robot class only depends on working, so it has one responsibility within its own class. The Robot class doesn�t depend on any other classes for functionality. 

* Interface Segregation Principle - Splitting the two functionality interfaces called Eat and iWorker is more efficient when adding the Robot class because robot workers do not eat. If the interfaces were combined, the Robot class would go through unnecessary eating. 
 
#### Travis Union - Created Two seperate interfaces (Eat and iWorker)

1. Eat interface: 

 * Interface Segregation Principle � This principle splits up the original interface into two separate interfaces that have one specific task, making them �thin� as opposed to �fat.� I.e. a robot can�t eat, but a worker can.

 * Open Close Principle � The Eat interface is open to extension so that workers can use the eat method.

 * Single Responsibility Principle � The new Eat interface initializes an eat method only.

2. iWorker interface: 

 * Interface Segregation Principle - This principle splits up the original interface into two separate interfaces that have one specific task, making them �thin� as opposed to �fat.� I.e. a worker can eat but a robot can�t.

 * Open Close Principle � The iWorker interface is open to extension so that workers can use the work method.

 * Single Responsibility Principle � The new iWorker interface initializes a work method only.

#### Tory Brewer - Created Manage.java and ThreeSolidMain.java

1. Manage

 * Open Close - The separate class for the manager allows for extension however is closed to modifications from the main program.
 * Single Responsibility - Responsible for only the worker its managing.
 * Interface Segregation - N/A
 
2. ThreeSolidMain

 * Open Close - Main is open for extension ie new objects.
 * Single Responsibility - N/A
 * Interface Segregation - Interfaces are segregated in main.
 
#### How to run the program
  
 1. First Install Homebrew/Chocolatey depending on your OS
 
 2. Install Ant
 
 3. path to the file directory
 
 4. run commands in this order, ant clean, ant compile, ant jar, then ant run to run the program